import { Stack, StackProps, Construct } from 'monocdk'
import { WaldoInfrastructure } from './waldo-infra'

export class WaldoStack extends Stack {
  private infra: WaldoInfrastructure

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props)

    this.infra = new WaldoInfrastructure(this, 'waldo-infra', {
      deviceName: 'WaldoDevice'
    })
  }

  getInfrastructure = (): WaldoInfrastructure => this.infra
}
