import { CfnOutput, Construct, CustomResource, Duration } from 'monocdk'
import { CfnPolicy, CfnThing } from 'monocdk/aws-iot'
import { Function, Runtime, Code } from 'monocdk/aws-lambda'
import { LambdaIntegration, LambdaRestApi } from 'monocdk/lib/aws-apigateway'
import {
  ManagedPolicy,
  PolicyDocument,
  PolicyStatement,
  Role,
  ServicePrincipal
} from 'monocdk/lib/aws-iam'
import { Secret } from 'monocdk/lib/aws-secretsmanager'
import {
  AwsCustomResource,
  AwsCustomResourcePolicy,
  PhysicalResourceId
} from 'monocdk/lib/custom-resources'

export interface WaldoInfrastructureProps {
  deviceName: string
}

export class WaldoInfrastructure {
  private device: CfnThing
  private certPolicy: CfnPolicy
  private configLambda: Function
  private configCustomResource: CustomResource
  private authTokenSecret: Secret
  private apiLambda: Function
  private api: LambdaRestApi
  private iotEndpoint: string
  private twilioWebhook: string

  constructor(scope: Construct, id: string, props: WaldoInfrastructureProps) {
    this.device = new CfnThing(scope, 'WaldoDevice', {
      thingName: props.deviceName
    })

    // You'll need to manually create a certificate for the device and
    // attach this policy to it. Remember to download the cert files
    // when you create it; you can't get them back later and would need
    // to create a new cert.
    this.certPolicy = new CfnPolicy(scope, 'WaldoDeviceCertPolicy', {
      policyName: 'WaldoDeviceCertificatePolicy',
      policyDocument: {
        Version: '2012-10-17',
        Statement: {
          Effect: 'Allow',
          Action: [
            'iot:Connect',
            'iot:Publish',
            'iot:Subscribe',
            'iot:Receive',
            'iot:GetThingShadow'
          ],
          Resource: '*' // TODO restrict this
        }
      }
    })

    const endpointResource = new AwsCustomResource(scope, 'IoTEndpoint', {
      onCreate: {
        service: 'Iot',
        action: 'describeEndpoint',
        physicalResourceId: PhysicalResourceId.fromResponse('endpointAddress'),
        parameters: {
          endpointType: 'iot:Data-ATS'
        }
      },
      policy: AwsCustomResourcePolicy.fromSdkCalls({
        resources: AwsCustomResourcePolicy.ANY_RESOURCE
      })
    })

    this.iotEndpoint = endpointResource.getResponseField('endpointAddress')

    this.configLambda = new Function(scope, 'WaldoDeviceConfigurationLambda', {
      functionName: 'WaldoDeviceConfigurationLambda',
      runtime: Runtime.NODEJS_14_X,
      code: Code.fromAsset('build/waldo'),
      handler: 'index.configLambdaHandler',
      timeout: Duration.seconds(10),
      role: new Role(scope, 'CustomResourceLambdaRole', {
        roleName: 'WaldoDeviceConfigurationLambdaRole',
        description: 'IAM role used by the WaldoDeviceConfigurationLambda',
        assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
        managedPolicies: [
          ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'),
          ManagedPolicy.fromAwsManagedPolicyName('AWSIoTFullAccess'),
          ManagedPolicy.fromAwsManagedPolicyName('AWSIoTDataAccess')
        ]
      }),
      environment: {
        WaldoDeviceName: props.deviceName,
        IotEndpoint: this.iotEndpoint
      }
    })

    this.configCustomResource = new CustomResource(scope, 'WaldoDeviceConfiguration', {
      serviceToken: this.configLambda.functionArn
    })
    this.configCustomResource.node.addDependency(this.device)

    /**
     * This secret will be empty; a Twilio auth token must be set as the value
     * before API requests will succeed.
     */
    this.authTokenSecret = new Secret(scope, 'TwilioAuthTokenSecret', {
      secretName: 'TwilioAuthTokenSecret',
      description:
        'Safely stores your Twilio auth token, used to validate that incoming requests come from Twilio.'
    })

    this.apiLambda = new Function(scope, 'WaldoApiLambda', {
      functionName: 'WaldoApiLambda',
      runtime: Runtime.NODEJS_14_X,
      code: Code.fromAsset('build/waldo'),
      handler: 'index.apiLambdaHandler',
      timeout: Duration.seconds(10),
      role: new Role(scope, 'WaldoApiLambdaRole', {
        roleName: 'WaldoApiLambdaRole',
        description: 'IAM role used by the WaldoApiLambda',
        assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
        managedPolicies: [
          ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'),
          ManagedPolicy.fromAwsManagedPolicyName('AWSIoTDataAccess')
        ],
        inlinePolicies: {
          GetTwilioAuthToken: new PolicyDocument({
            statements: [
              new PolicyStatement({
                actions: ['secretsmanager:DescribeSecret', 'secretsmanager:GetSecretValue'],
                resources: [this.authTokenSecret.secretArn]
              })
            ]
          })
        }
      }),
      environment: {
        WaldoDeviceName: props.deviceName,
        IotEndpoint: this.iotEndpoint,
        AuthTokenSecret: this.authTokenSecret.secretArn,
        /**
         * If this value is set, for any message that is <= 16
         * characters (one line), the message will be the second
         * line and this will be the first. If your message is
         * longer than 16 characters, this will not be added.
         * 
         * If you don't want this behaviour, just delete this value.
         */
        ShortMessageFirstLine: 'Where\'s Waldo?'
      }
    })

    this.api = new LambdaRestApi(scope, 'WaldoApi', {
      description: 'An API to update the AWS IoT device shadow for the Waldo device.',
      handler: this.apiLambda,
      proxy: false
    })
    const updateShadow = this.api.root.addResource('waldo')
    updateShadow.addMethod('POST', new LambdaIntegration(this.apiLambda))

    this.twilioWebhook = `${this.api.url}waldo`

    new CfnOutput(scope, 'IotEndpoint', {
      exportName: 'iotEndpoint',
      description: "Your account's IoT endpoint.",
      value: this.iotEndpoint
    })

    new CfnOutput(scope, 'ApiUrl', {
      exportName: 'twilioWebhook',
      description: 'The webhook that Twilio should use to send SMS messages.',
      value: this.twilioWebhook
    })

    new CfnOutput(scope, 'SecretArn', {
      exportName: 'seecretArn',
      description: 'The ARN of the SecretsManager secret that will store your Twilio auth token',
      value: this.authTokenSecret.secretArn
    })
  }

  getDevice = (): CfnThing => this.device
  getCertificatePolicy = (): CfnPolicy => this.certPolicy
  getConfigurationLambda = (): Function => this.configLambda
  getConfigurationCustomResource = (): CustomResource => this.configCustomResource
  getAuthTokenSecret = (): Secret => this.authTokenSecret
  getApiLambda = (): Function => this.apiLambda
  getApi = (): LambdaRestApi => this.api
  getIotEndpoint = (): string => this.iotEndpoint
  getTwilioWebhook = (): string => this.twilioWebhook
}
