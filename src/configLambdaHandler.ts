const AWS = require('aws-sdk')
const response = require('cfn-response')

/**
 * Creates a shadow for the Waldo device in AWS IoT
 */
export const handler = async (event: any, context: any) => {
  const sendResponse = (success: boolean) => {
    const status = success ? 'SUCCESS' : 'FAILED'
    console.log(`Sending CloudFormation response: ${status}`)
    return new Promise(() => {
      response.send(event, context, status)
    })
  }
  try {
    // Send a SUCCESS response for delete requests without doing anything
    if (event.RequestType == 'Delete') {
      await sendResponse(true)
      return
    }
    const iotData = new AWS.IotData({
      endpoint: process.env.IotEndpoint,
      region: process.env.AWS_REGION || 'us-east-1',
      maxRetries: 3
    })
    const response = await iotData
      .updateThingShadow({
        thingName: process.env.WaldoDeviceName,
        payload: JSON.stringify({
          state: {
            desired: {
              location: 'The Make Fun Stuff Workshop'
            }
          }
        })
      })
      .promise()
    console.log(`Update shadow response: ${JSON.stringify(response, null, 2)}`)
  } catch (error) {
    console.log('Something went wrong!')
    console.log(error)
    await sendResponse(false)
    return
  }
  await sendResponse(true)
  return
}
