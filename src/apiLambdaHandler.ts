import { allowedPhoneNumbers } from "./phoneNumbers";
import { IotData, SecretsManager } from "aws-sdk";
import { validateRequest } from "twilio";

const iot = new IotData({
  region: process.env.AWS_REGION || "us-east-1",
  endpoint: process.env.IotEndpoint,
  maxRetries: 3,
});

const secretsManager = new SecretsManager({
  region: process.env.AWS_REGION || "us-east-1",
  maxRetries: 3,
});

const getXmlResponse = (body?: string) => {
  return `<Response>${
    body ? `<Message><Body>${body}</Body></Message>` : ""
  }</Response>`;
};

// cache this to avoid calling SecretsManager for every request
let authToken: string | undefined;

const getAuthToken = async (): Promise<string> => {
  if (!authToken) {
    const response = await secretsManager
      .getSecretValue({
        SecretId: process.env.AuthTokenSecret!,
      })
      .promise();
    authToken = response.SecretString;
  }
  return authToken!;
};

/**
 * Handles API requests from Twilio to update the IoT shadow of the device
 */
export const handler = async (event: any, context: any, callback: any) => {
  console.log(`Event: ${JSON.stringify(event, null, 2)}`);
  try {
    // TODO use some library for this
    const body: Record<string, string> = event.body
      .split("&")
      .reduce((acc: Record<string, string>, param: string) => {
        const spIndex = param.indexOf("=");
        return {
          ...acc,
          [param.slice(0, spIndex)]: decodeURIComponent(
            param.slice(spIndex + 1).replace(/\+/g, " ")
          ),
        };
      }, {});
    console.log(`Body: ${JSON.stringify(body, null, 2)}`);

    const url = `https://${event.headers.Host}/prod/waldo`;

    if (
      !validateRequest(
        await getAuthToken(),
        event.headers["X-Twilio-Signature"],
        url,
        body
      )
    ) {
      throw Error("Unable to validate incoming request");
    }

    const sender = body.From;
    // only bother with first 32 characters (2x16 display)
    const truncatedMessage = body.Body.trim().slice(0, 32);
    const firstLine = process.env.ShortMessageFirstLine;
    const message =
      !!firstLine && truncatedMessage.length <= 16
        ? `${firstLine}|${truncatedMessage}`
        : truncatedMessage;

    if (!allowedPhoneNumbers.includes(sender)) {
      throw Error(`Invalid sender: ${sender}`);
    }
    console.log(`Updating shadow to "${message}"`);
    const response = await iot
      .updateThingShadow({
        thingName: process.env.WaldoDeviceName!,
        payload: JSON.stringify({
          state: {
            desired: {
              location: message,
            },
          },
        }),
      })
      .promise();
    console.log(`IoT response: ${JSON.stringify(response, null, 2)}`);
    // send success response
    return callback(null, {
      statusCode: 200,
      headers: {
        "Content-Type": "application/xml",
      },
      body: getXmlResponse(),
    });
  } catch (error) {
    // send SMS error response
    console.log(error);
    return callback(null, {
      statusCode: 200,
      headers: {
        "Content-Type": "application/xml",
      },
      body: getXmlResponse("Unable to update message"),
    });
  }
};
