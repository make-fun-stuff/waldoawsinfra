/**
 * Messages from any phone number that is NOT in this list
 * will not be processed. Numbers must be in the format
 * '+12223334444'.
 */
export const allowedPhoneNumbers: string[] = []
